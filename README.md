# Superhero (plugin for Wordpress)

This plugin displays the superhero card in your Wordpress content.

## Installation

1) Download the [superhero.zip](https://bitbucket.org/devxteros/superhero/get/ff9293d24594.zip) file.

2) Upload the .zip file using the plugin installation tool included in Wordpress. (If you prefer you can instead copy the "superhero" folder into your plugins / wp-content / plugins directory)

3) Activate the plugin


## How to use [shortcode]

```php
[superhero id="644"]
```
Implement the shortcode [superhero id="**644**"] where the parameter **id** represents the code of the favorite superhero. You can get the code list from [superheroapi](https://superheroapi.com/ids.html)

![Demo](https://bitbucket.org/devxteros/superhero/raw/0357ac2dc5538d2ffce0248de3141224c2b249df/superhero.gif)


## License
[MIT](https://choosealicense.com/licenses/mit/)