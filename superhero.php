<?php
/**
 * Plugin Name:     SuperHero
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     The plugin shows a superhero card in wordpress using a short code.
 * Author:          Juan Manuel Lora
 * Author URI:      mailto:juanmalora@gmail.com
 * Text Domain:     superhero
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Superhero
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

define('SUPERHERO_API', "https://superheroapi.com/api");

add_action('init', 'shortcodes_init');

function shortcodes_init(){
    add_shortcode( 'superhero', 'shortcode_handler_superhero' );
}

function shortcode_handler_superhero( $atts, $content = null){
    $params = shortcode_atts( array(
		'id' => '0',
		'theme' => 'light',
    ), $atts );


    $cache_data = get_transient("superhero-".$params["id"]);
    

    if( !$cache_data ){
        
        $access_token = "10159068628957978";
        $url = SUPERHERO_API."/".$access_token."/".$params['id'];    
        $response = wp_remote_get( $url);

        if ( is_wp_error( $response ) ) {
            $error_message = $response->get_error_message();
            return $error_message;
        } else {        
            $data = json_decode($response['body'], true);
    
            if($data["response"] == "error"){
                return $data["error"];
            }
    
            if($data["response"] == "success"){
                set_transient("superhero-".$params["id"], $data, 30 * DAY_IN_SECONDS);
            }
        }

    }else{

        $data = $cache_data;
        
    }

    $card = render_card_superhero($data);
    return $card;
}


function render_card_superhero( $hero ){
    
    $html = '<div class="card-container">
    <div class="card">
        <div class="front">
            <div class="cover">
                <img src="'.$hero['image']['url'].'"/>
            </div>
            <div class="user">
                <img class="img-circle" src="'.$hero['image']['url'].'"/>
            </div>
            <div class="content">
                <div class="main">
                    <h3 class="name">'.$hero['name'].'</h3>
                    <p class="profession">'.$hero['biography']['full-name'].'</p>
                    <p class="text-center">"'.$hero['connections']['group-affiliation'].'"</p>
                </div>
                <div class="footer">
                    <i class="fa fa-mail-forward"></i> '.$hero['biography']['publisher'].'
                </div>
            </div>
        </div> 
        <div class="back">
            <div class="header">
                <h5 class="motto">'.$hero['biography']['first-appearance'].'</h5>
            </div>
            <div class="content">
                <div class="main">
                    <h4 class="text-center">'.$hero['work']['occupation'].'</h4>
                    <p class="text-center">'.$hero['work']['base'].'</p>

                    <div class="stats-container">
                        <div class="stats">
                            <h4>'.$hero['powerstats']['speed'].'</h4>
                            <p>
                                Speed
                            </p>
                        </div>
                        <div class="stats">
                            <h4>'.$hero['powerstats']['power'].'</h4>
                            <p>
                                Power
                            </p>
                        </div>
                        <div class="stats">
                            <h4>'.$hero['powerstats']['intelligence'].'</h4>
                            <p>
                            Intelligence
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="footer">
                <div class="social-links text-center">
                    <a href="#" class="facebook"><i>'.$hero['appearance']['gender'].'</i></a>
                    <a href="#" class="facebook"><i>'.$hero['appearance']['height'][1].'</i></a>
                    <a href="#" class="facebook"><i>'.$hero['appearance']['weight'][1].'</i></a>
                </div>
            </div>
        </div>  
    </div> 
</div>';
    return $html;
}



/** ADD CSS & JS */
function add_superhero_scripts() {

    wp_register_style( 'bootstrap', plugins_url( 'assets/css/bootstrap.css', __FILE__ ), false);
    wp_register_style( 'pe-icon-7-stroke', plugins_url( 'assets/css/pe-icon-7-stroke.css', __FILE__ ), false);
    wp_register_style( 'rotating-card', plugins_url( 'assets/css/rotating-card.css', __FILE__ ), false);
    wp_register_style( 'font-awesome', plugins_url( 'assets/css/font-awesome/css/font-awesome.min.css', __FILE__ ), false);
    wp_enqueue_style ( 'bootstrap' );
    wp_enqueue_style ( 'pe-icon-7-stroke' );
    wp_enqueue_style ( 'rotating-card' );
    wp_enqueue_style ( 'font-awesome' );

}

add_action( 'wp_enqueue_scripts', 'add_superhero_scripts' );


